var assert = require('assert');
const ops = require('../operation');

describe('getLargestNumberPossible', function () {

  it('Should return empty array when obj parameter does not have data', async function() {
    let arr = [];
    let status = await ops.getLargestNumberPossible( arr );
    assert.equal( status[0].ERR_MSG, 'Empty array.' );
  });

  it('Should return numbers only when one of the values from the array is a character/string', async function () {
    let arr = [ 34,255,'a',1,15,6,44 ];
    let status = await ops.getLargestNumberPossible( arr );
    assert.equal( status[0].ERR_MSG, 'Number/s only.' );
  });

  it('Should return positive numbers only when one of the values from the array is a negative number', async function () {
    let arr = [ 34,255,-1,15,6,44 ];
    let status = await ops.getLargestNumberPossible( arr );
    assert.equal( status[0].ERR_MSG, 'Positive number/s only.' );
  });

  it('Should return correct output', async function () {
    let arr = [ 34,255,1,15,6,44 ];
    let status = await ops.getLargestNumberPossible( arr );
    assert.equal( status, "64434255151" );
  });

});