var assert = require('assert');
const ops = require('../operation');

describe('getLargestNumberPossible', function () {

  /** TODO:
   * 1. check if obj is an array
   * 2. check if all values are number
   * 3. check if n is integer
   * 4. check if output is correct
  **/
  it('Should return empty array when obj parameter does not have data', async function() {
    let arr = [];
    let status = await ops.getNthNumberFromEnd( arr );
    assert.equal( status.ERR_MSG, 'Empty array.' );
  });

  it('Should return number/s only when n param is not an integer', async function() {
    let arr = [ 34,255,1,15,6,44 ];
    let status = await ops.getNthNumberFromEnd( arr, 'a' );
    assert.equal( status.ERR_MSG, 'Number/s only.' );
  });

  it('Should return numbers only when one of the values from the array is a character/string', async function () {
    let arr = [ 34,255,'a',1,15,6,44 ];
    let status = await ops.getNthNumberFromEnd( arr, 2 );
    assert.equal( status.ERR_MSG, 'Number/s only.' );
  });

  it('Should return correct output', async function () {
    let arr = [ 34,255,1,15,6,44 ];
    let status = await ops.getNthNumberFromEnd( arr, 2 );
    assert.equal( status.two, 44 );
  });

});