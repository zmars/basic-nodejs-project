This is a basic Node project with unit testing.

---

## Prerequisites

1. Node v8.8.0
2. NPM v5.4.2
3. Mocha v5.2.0

---

## Run Unit Testing

1. From your console, go to directory of your project
2. Type "npm test" > enter
3. You will see from the console the result of the unit testing