
module.exports = {
  /** 
   * @description This function sorts a number array and returns the Nth largest element from the end.
   * 
   * @param obj - number array
   * @param input - index of value to return
   * 
   * @example getNthNumberFromEnd( [ 2,6,7,3,5 ], 2 );
   * @returns 6
   * 
  **/
  getNthNumberFromEnd: async function ( obj, input ) {
    let finalOutput;

    if( ((finalOutput = await utils.isArrayNotEmpty( obj )) == true ) && 
        ((finalOutput = await utils.isNumeric( input )) == true ) && 
        ((finalOutput = await utils.isNumeric( obj )) == true ) )  {
      finalOutput = {};
      await caseOneImplementation.init( obj, input );
      finalOutput.one = await caseOneImplementation.compute();
      finalOutput.two = await caseOneImplementation.computejs();
    }else {
      finalOutput = {
        "ERR_MSG": finalOutput
      };
    }
  
    return finalOutput;
  },

  /** 
   * @description This function sorts a number array and form the largest number possible
   * 
   * @param obj - number array
   * 
   * @example getLargestNumberPossible( [ 2,6,7,3,5 ] );
   * @returns 676532
   * 
  **/
  getLargestNumberPossible: async function( obj ) {
     let finalOutput;

    if( (finalOutput = await utils.validateNumberArr( obj )) == true ) {
      await caseTwoImplementation.init( obj );
      finalOutput = await caseTwoImplementation.compute();
    }
  
    return finalOutput;
  }
}


/** CaseOne's Actual Implementation **/
const caseOneImplementation = {
  container: [], // number array container
  input: 0, // index of value

  /** Initialize values from input **/
  init: async function ( _obj, _input ) {
    caseOneImplementation.container   = _obj;
    caseOneImplementation.input       = _input;
  },

  /** Algorithm **/
  compute: async function () {
    const _container  = caseOneImplementation.container;
    const _input      = caseOneImplementation.input;

    for( var x = ( _container.length - 1 ); x >= 0; x-- ) {
      for( var y = 1; y <= x; y++ ) {
        if( _container[ y - 1 ] > _container[ y ] ) {
          let temp              = _container[ y - 1 ];
          _container[ y - 1 ]   = _container[ y ];
          _container[ y ]       = temp;
        }
      }
    }

    const output = _container[ _container.length - ( _input ) ];

    return output;
  },

  /** Algorithm [utilizing JS functions] **/
  computejs: async function () {
    const _container  = caseOneImplementation.container;
    const _input      = caseOneImplementation.input;

    _container.sort((a, b) => a - b );

    const output = _container[ _container.length - ( _input ) ];

    return output;
  }
};

/** CaseTwo's Actual Implementation **/
const caseTwoImplementation = {
  container: [], // number array container

  /** Initialize values from input **/
  init: async function ( _obj ) {
    caseTwoImplementation.container   = _obj;
  },

  /** Algorithm **/
  compute: async function () {
    const _container  = caseTwoImplementation.container;

    _container.sort( function compare( x, y ) {
      var xy = '' + x + y;
      var yx = '' + y + x;
      return xy > yx ? -1 : 1;
    });

    const output = _container.join('');

    return output;
  }
}

/** Basic Utils **/
const utils = {
  validateNumberArr: async function  ( obj ) {
    let status = {};
    let err = [];
    let isValid = true;
    
    status.HAS_ARRAY_VALUES = await utils.isArrayNotEmpty( obj );
    status.POSITIVE_ONLY = await utils.isAllPositiveNumbers( obj );
    status.NUMERIC_ONLY = await utils.isNumeric( obj );

    Object.keys( status ).forEach( key => {
      if( status[ key ] !== true ) {
        err.push({
          "ERR_MSG": status[ key ]
        });
        isValid = false;
      }
    });

    let finalStatus = ( isValid == true ) ? true : err;

    return finalStatus;
  },

  isAllPositiveNumbers: async function( obj ) {
    let status = true;
    try {
      for( let x = 0; x < obj.length; x++ ) {
        status = ( obj[ x ] < 0 ) ? false : true;
        if(!status) throw "Positive number/s only."
      }
    }catch ( err ) {
      status = err;
    }

    return status;
  },

  isNumeric: async function( obj ) {
    let status = true;
    try {
      if(typeof( obj ) == "object") {
        for( let x = 0; x < obj.length; x++ ) {
          status = ( isNaN( obj[ x ] ) ) ? false : true;
          if(!status) throw "Number/s only."
        }
      }else {
        status = ( isNaN( obj ) ) ? false : true;
        if(!status) throw "Number/s only."
      }
    }catch ( err ) {
      status = err;
    }

    return status;
  },

  isArrayNotEmpty: async function( obj ) {
    let status = true;
    try {
      status = ( obj.length <= 0 ) ? false : true;
      if(!status) throw "Empty array."
    }catch ( err ) {
      status = err;
    }

    return status;
  }
};