
const ops = require("./operation");
console.log("Module is loaded", ops);

async function run() {
  let arr = [ 34,255,1,15,6,44, 'a' ];
  
  const responseOne = await ops.getNthNumberFromEnd( arr, 4 ); 
  const responseTwo = await ops.getLargestNumberPossible( arr );

  console.log("\n\nRESPONSE ONE", responseOne);  //value of nth index from the sorted array
  console.log("RESPONSE TWO", responseTwo); //largest number
}

run();